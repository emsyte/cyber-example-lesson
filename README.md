# Example lesson helm chart

This repository contains an example helm chart that a teacher might deploy as part of a lesson.


## Manual Installation

Use helm to perform a manual installation:

```
helm install . -n student-<student_id>
```

The `-n student-<student_id>` section provides a name to the release - the student ID is
used so that the release can easily be found & deleted later.

The helm Chart's name should be as such:

```
lesson-<lesson_id>
```

This produces nice internal endpoints such as the following:

```
student-00070e90-511c-44ab-9291-4e25c667cff3-lesson-10ef9d1b-a0a1-43e3-a699-794a3d16f063.lessons
```

Which allows us to easily discover which pod belongs to which lesson & which student.
